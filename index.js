var Device = require('zetta-device');
var util = require('util');


var configDefaults = {
  quiet: false,
  heatingSetpoint: 25,
  coolingSetpoint: 25,
  startingTempMin: 20,
  startingTempMax: 30,
  changeIntervalMin: 2500,
  changeIntervalMax: 8000
};

/**
 * @param {string} name - Name defaults to 'thermostat' if not provided
 * @param {string | object} options - If string, run through `JSON.parse`
 * @param {boolean} options.quiet - Disable all logging
 * @param {number} options.heatingSetpoint - Defaults to 25.0
 * @param {number} options.coolingSetpoint - Defaults to 25.0
 * @param {number} options.startTempMin - Defaults to 20
 * @param {number} options.startTempMax - Defaults to 30
 * @param {number} options.changeIntervalMin - Defaults to 2500
 * @param {number} options.changeIntervalMax - Defaults to 8000
 */
var Thermostat = module.exports = function(name, options) {
  Device.call(this);
  this.name = name || 'thermostat';

  var config = getConfig(options);
  if(config.quiet) {
    // quiet: `log` => `noop`
    log = function(){}
  }

  this.currentTemperature = randomTemp(config.startingTempMin, config.startingTempMax);
  this.heatingSetpoint = config.heatingSetpoint;
  this.coolingSetpoint = config.coolingSetpoint;
  this.mode = 'Cool'
  this.activeMode = '';
  this.relayState = '';

  this._drift = 0.5;
  this._tempChange = 0.2;

  var minTimeout = config.changeIntervalMin;
  var maxTimeout = config.changeIntervalMax;

  this._tick(function(){
    this._scheduleTick(minTimeout, maxTimeout);
  }.bind(this));

}

util.inherits(Thermostat, Device);

Thermostat.prototype.init = function(config) {
  config
    .name(this.name)
    .type('thermostat')
    .monitor('currentTemperature')
    .monitor('heatingSetpoint')
    .monitor('coolingSetpoint')
    .monitor('activeMode')
    .monitor('relayState')
    .monitor('mode')
    .state('ready')
    .when('ready', {allow:['SetHeatingSetpoint','SetCoolingSetpoint','SetMode']})
    .map('SetHeatingSetpoint', this.setHeatingSetpoint, [{name:'temperature',type:'number'}])
    .map('SetCoolingSetpoint', this.setCoolingSetpoint, [{name:'temperature',type:'number'}])
    .map('SetMode', this.setMode, [
      {
        name:'mode',
        type:'radio',
        value: [
          {value:'Off'},
          {value:'Cool'},
          {value:'Heat'}
        ]
      }
    ])
}

Thermostat.prototype.setHeatingSetpoint = function(temp, cb) {
  this.heatingSetpoint = Number(temp);
  cb();
}

Thermostat.prototype.setCoolingSetpoint = function(temp, cb) {
  this.coolingSetpoint = Number(temp);
  cb();
}

Thermostat.prototype.setMode = function(mode, cb) {
  this.mode = mode;
  cb();
}

Thermostat.prototype._scheduleTick = function(minTime, maxTime) {
  var when = randomBound(minTime, maxTime);
  var nextTickFn = this._scheduleTick.bind(this, minTime, maxTime);
  var tickFn = this._tick.bind(this, nextTickFn);
  setTimeout(tickFn, when);
};

Thermostat.prototype._tick = function(cb) {
  var current = this.currentTemperature;
  var mode = this.mode;
  var relay = this.relayState;

  var jitter = randomTemp(0,0.1);
  if(this._tempChange < 0) {
    jitter *= -1;
  }

  current += (this._tempChange + jitter);
  current = Number(current.toFixed(2));
  if(current > 100) current = 100;
  else if(current < 0) current = 0;

  log(this.name + ': Temp changed to ' + current);
  this.currentTemperature = current;

  switch(mode) {
    case 'Cool':
      // cool if the current temp is > than the setpoint and drift
      if(current > (this.coolingSetpoint + this._drift)) {
        // if we are already cooling, do nothing
        if( relay.indexOf('Cool') == -1 ) {
          log('starting cool');
          this._tempChange = -0.2;
          this.relayState = 'Cool,Fan';
          this.activeMode = 'Cool';
        }
      } else if(current < (this.coolingSetpoint - this._drift)) {
        if( relay != '' ) {
          log('stopping cool');
          this._tempChange = 0.2;
          this.relayState = '';
          this.activeMode = 'Idle';
        }
      }
      break;
    case 'Heat':
      // cool if the current temp is > than the setpoint and drift
      if(current < (this.heatingSetpoint - this._drift)) {
        // if we are already cooling, do nothing
        if( relay.indexOf('Heat') == -1 ) {
          log('staring heat');
          this._tempChange = 0.2;
          this.relayState = 'Heat,Fan';
          this.activeMode = 'Heat';
        }
      } else if(current > (this.heatingSetpoint + this._drift)) {
        if( relay != '' ) {
          log('stopping heat');
          this._tempChange = -0.2;
          this.relayState = '';
          this.activeMode = 'Idle';
        }
      }
      break;
  }

  cb();
}

function randomBound(low,high) {
  return Math.random() * (high-low) + low;
}

function randomTemp(low, high) {
  return Number(randomBound(low,high).toFixed(2));
}

function log(message) {
  console.log(message);
}

function getConfig(options) {
  var result = {};

  options = options || {};
  if(typeof options === 'string') {
    try { options = JSON.parse(options); }
    catch(err) { console.log(err); }
  }

  Object.keys(configDefaults).forEach(function(prop){
    var override = options.hasOwnProperty(prop);
    result[prop] = override ? options[prop] : configDefaults[prop];
  });

  return result;
}